'use strict';
var yeoman = require('yeoman-generator');
var chalk  = require('chalk');
var yosay  = require('yosay');
var uuid   = require('uuid');

var gruntBuildTasks = ['copy:fonts', 'copy:modernizr', 'icons', 'svgmin:images', 'newer:imagemin', 'sass_globbing:dist', 'sass:dist', 'postcss:dist', 'browserify:dist', 'exorcise', 'uglify'];

module.exports = yeoman.generators.Base.extend({
	initializing: function () {
		this.pkg = require('../package.json');
	},

	slugProjectName: function () {
		return this.projectName
			.toLowerCase()
			.replace(/ /g,'-')
			.replace(/[^\w-]+/g,'')
			;
	},

	prompting: function () {
		var done = this.async();

		// Have Yeoman greet the user.
		this.log(yosay(
			'Welcome to the neat ' + chalk.red('Bluegg') + ' generator!'
		));

		var prompts = [{
			name: 'projectName',
			message: 'Project name:',
			default: 'My Project'
		}, {
			name: 'projectUrl',
			message: 'Project URL:',
			default: 'project.co.uk'
		}, {
			name: 'projectDescription',
			message: 'Project description:',
			default: 'Project 2016'
		}, {
			name: 'projectAuthor',
			message: 'Project author:',
			default: 'Author Name'
		}, {
			name: 'projectAuthorEmail',
			message: 'Project author email:',
			default: 'author@bluegg.co.uk'
		}, {
			name: 'projectType',
			message: 'What sort of project will this be?',
			type: 'rawlist',
			choices: ['Craft CMS', 'Laravel', 'Static'],
			default: 'Craft CMS'
		}, {
			name: 'ansible',
			message: 'Install Ansible?',
			type: 'confirm',
			default: 'Yes'
		}];

		this.prompt(prompts, function (props) {
			this.projectName        = props.projectName;
			this.projectUrl         = props.projectUrl;
			this.projectDescription = props.projectDescription;
			this.projectAuthor      = props.projectAuthor;
			this.projectAuthorEmail = props.projectAuthorEmail;
			this.projectType        = props.projectType;
			this.ansible            = props.ansible;
			this.projectLocalUrl    = props.projectUrl.replace(/(.com|.co.uk|.org)/g, '.local');
			done();
		}.bind(this));
	},

	writing: {

		app: function () {

			this.fs.copyTpl(
				this.templatePath('_package.json'),
				this.destinationPath('package.json'),	{
					projectName: this.projectName,
					projectAuthor: this.projectAuthor,
					projectDescription: this.projectDescription,
					slugName: this.slugProjectName()
				}
			);

			this.fs.copy(
				this.templatePath('grunt/*'),
				this.destinationPath('grunt/')
			);

			// Determine where BrowserSync checks
			var syncSrc = 'public/**/*.html';
			if (this.projectType == 'Craft CMS')
			{
				syncSrc = 'craft/templates/**/*.twig';
			}
			if (this.projectType == 'Laravel')
			{
				syncSrc = 'resources/views/**/*.php';
			}

			// Determine location of web root
			var appDir = 'public';

			// Determine file that cache-busting amends
			var headPartial = 'resources/markup/_partials/head-scripts.twig';
			var footPartial = 'resources/markup/_partials/foot-scripts.twig';

			if (this.projectType == 'Static')
			{
				gruntBuildTasks.push('twigRender');
			}
			if (this.projectType == 'Craft CMS')
			{
				appDir      = 'craft/web';
				headPartial = 'craft/templates/_partials/head-scripts.twig';
				footPartial = 'craft/templates/_partials/foot-scripts.twig';
			}
			if (this.projectType == 'Laravel')
			{
				headPartial = 'resources/partials/head-scripts.blade.php';
				footPartial = 'resources/partials/foot-scripts.blade.php';
			}

			this.fs.copyTpl(
				this.templatePath('_gruntfile.js'),
				this.destinationPath('gruntfile.js'), {
					title: this.projectName,
					url: this.projectLocalUrl,
					appDir: appDir,
					browserSyncHtmlSrc: syncSrc,
					headPartial: headPartial,
					footPartial: footPartial,
					gruntBuildTasks: gruntBuildTasks.join("', '")
				}
			);
		},

		projectfiles: function ()
		{
			this.fs.copy(
				this.templatePath('editorconfig'),
				this.destinationPath('.editorconfig')
			);
			this.fs.copy(
				this.templatePath('jshintrc'),
				this.destinationPath('.jshintrc')
			);
			this.fs.copy(
				this.templatePath('_eslintrc.json'),
				this.destinationPath('.eslintrc.json')
			);
			this.fs.copy(
				this.templatePath('files/Vagrantfile'),
				this.destinationPath('Vagrantfile')
			);

			var inventory = '';

			if (this.projectType == 'Craft CMS')
			{
				inventory = 'craft';

				this.fs.copy(
					this.templatePath('craft/craft-install.sh'),
					this.destinationPath('install.sh')
				);
			}
			else if (this.projectType == 'Laravel')
			{
				inventory = 'laravel';

				this.fs.copy(
					this.templatePath('laravel/laravel-install.sh'),
					this.destinationPath('install.sh')
				);
			}
			else
			{
				inventory = 'static';

				this.fs.copy(
					this.templatePath('resources/**/*'),
					this.destinationPath('resources/')
				);
				this.fs.copy(
					this.templatePath('gitignore'),
					this.destinationPath('.gitignore')
				);
			}

			this.log("Copying Ansible install scripts... ---------------------------");

			if (this.ansible == true)
			{
				this.fs.copy(
					this.templatePath('ansible/ansible-install.sh'),
					this.destinationPath('ansible-install.sh')
				);
				this.fs.copy(
					this.templatePath('ansible/ansible-'+inventory+'.sh'),
					this.destinationPath('ansible-platform.sh')
				);
			}
		},
	},

	install: function () {

		var _this = this;

		this.log("Installing dependencies... ---------------------------");

		this.installDependencies({
			skipInstall: true
		});

		this.log("Dependencies installed! ---------------------------");

		if (this.ansible == true)
		{
			this.log("Installing Ansible... ---------------------------");

			this.spawnCommand('bash', ['ansible-install.sh'])
			.on('close', function(e)
			{
				// Remove install file
				_this.spawnCommand('rm', [
					'ansible-install.sh'
				]);

				_this.spawnCommand('bash', ['ansible-platform.sh'])
				.on('close', function(e)
				{
					// Remove install file
					_this.spawnCommand('rm', [
						'ansible-platform.sh'
					]);

					_this.log("Ansible installed! ---------------------------");
				});
			});
		}

		// Installing Craft stuff
		if (this.projectType == 'Craft CMS')
		{
			this.log("Installing Craft locally... ---------------------------");

			this.spawnCommand('bash', ['install.sh'])
			.on('close', function(e)
			{
				// Copy custom Craft files and resources over
				_this.fs.copy(
					_this.templatePath('craft/gitignore.txt'),
					_this.destinationPath('.gitignore')
				);
				_this.fs.copy(
					_this.templatePath('resources/images/**/*'),
					_this.destinationPath('resources/images/')
				);
				_this.fs.copy(
					_this.templatePath('resources/scripts/**/*'),
					_this.destinationPath('resources/scripts/')
				);
				_this.fs.copy(
					_this.templatePath('resources/styles/**/*'),
					_this.destinationPath('resources/styles/')
				);
				_this.fs.copy(
					_this.templatePath('resources/favicon.png'),
					_this.destinationPath('resources/favicon.png')
				);
				_this.fs.copy(
					_this.templatePath('craft/http-headers.twig'),
					_this.destinationPath('craft/templates/_partials/http-headers.twig')
				);
				_this.fs.copy(
					_this.templatePath('resources/markup/index.twig'),
					_this.destinationPath('craft/templates/_layouts/master.twig'),
					{
						process: function(content) {
							var newContent = content.toString().replace(/include '/g, "include '_partials/");
							var newContent = "{% include '_partials/http-headers' %}\n" + newContent;
							return newContent;
						}
					}
				);
				_this.fs.copy(
					_this.templatePath('resources/markup/_partials/**/*'),
					_this.destinationPath('craft/templates/_partials/')
				);
				_this.fs.copy(
					_this.templatePath('craft/index.twig'),
					_this.destinationPath('craft/templates/index.twig')
				);

				// Add directory for cms-assets
				_this.spawnCommand('mkdir', ['craft/web/cms-assets']);

				// Replace default general config with custom
				_this.spawnCommand('rm', ['craft/config/general.php'])
				.on('close', function(e)
				{
					_this.fs.copy(
						_this.templatePath('craft/config/general.php'),
						_this.destinationPath('craft/config/general.php')
					);
				});

				// Remove redundant files
				_this.spawnCommand('rm', [
					'readme.txt',
					'craft/templates/_layout.html',
					'craft/templates/404.html',
					'craft/templates/index.html'
				]);
				_this.spawnCommand('rm', [
					'-r',
					'craft/templates/news'
				]);
				_this.spawnCommand('rm', [
					'install.sh'
				]);

				// Create random string for Craft security key
				var strRand = uuid.v4();

				// If ansible present, add security key to webservers config
				if (_this.ansible == true)
				{
					// Set security key in Ansible group_vars

					var webserversPath = 'ansible/group_vars/webservers.yml';
					var webservers     = _this.fs.read(webserversPath);

					_this.spawnCommand('rm', [webserversPath])
					.on('close', function(e)
					{
						webservers = webservers.replace("{security_key}", strRand);
						_this.fs.write(webserversPath, webservers);

					});
				}

				// Set same security key in .env file
				var envPath = 'craft/.env';
				var envFile = _this.fs.read(envPath);

				_this.spawnCommand('rm', [envPath])
				.on('close', function(e)
				{
					envFile = envFile.replace(/SECURITY_KEY="(.*?)"/g, 'SECURITY_KEY="' + strRand + '"');
					_this.fs.write(envPath, envFile);
				});

				_this.log("Installing custom plugins! ---------------------------");

				_this.spawnCommand('composer', [
					'require',
					'mmikkel/cp-field-inspect'
				], {
					cwd: 'craft/'
				});
				_this.spawnCommand('composer', [
					'require',
					'studioespresso/craft-seeder'
				], {
					cwd: 'craft/'
				});
				_this.spawnCommand('composer', [
					'require',
					'craftcms/redactor'
				], {
					cwd: 'craft/'
				});
				_this.spawnCommand('composer', [
					'require',
					'ether/seo'
				], {
					cwd: 'craft/'
				});
				_this.spawnCommand('composer', [
					'require',
					'doublesecretagency/craft-inventory'
				], {
					cwd: 'craft/'
				});
				_this.spawnCommand('composer', [
					'require',
					'verbb/field-manager'
				], {
					cwd: 'craft/'
				});
				_this.spawnCommand('composer', [
					'require',
					'mmikkel/cp-clearcache'
				], {
					cwd: 'craft/'
				});
				_this.spawnCommand('composer', [
					'require',
					'topshelfcraft/environment-label'
				], {
					cwd: 'craft/'
				});
				_this.spawnCommand('composer', [
					'require',
					'aelvan/imager'
				], {
					cwd: 'craft/'
				});

				_this.log("Craft installed locally! ---------------------------");
			})
			.on('error', function(e)
			{
				_this.log("error!");
				_this.log(e);
			});
		}
		else if (this.projectType == 'Laravel')
		{
			this.log("Installing Laravel locally... ---------------------------");

			this.spawnCommand('bash', ['install.sh'])
			.on('close', function(e)
			{
				_this.fs.copy(
					_this.templatePath('resources/scripts/**/*'),
					_this.destinationPath('resources/assets/scripts/')
				);
				_this.fs.copy(
					_this.templatePath('resources/styles/scss/**/*'),
					_this.destinationPath('resources/assets/scss/')
				);
				_this.fs.copy(
					_this.templatePath('resources/images/*'),
					_this.destinationPath('public/images/')
				);

				_this.spawnCommand('rm', [
					'install.sh'
				]);

				_this.log("Laravel installed locally! ---------------------------");
			})
			.on('error', function(e)
			{
				_this.log("error!");
				_this.log(e);
			});
		}
	},

	end: function() {

		var _this = this;


	}
});
