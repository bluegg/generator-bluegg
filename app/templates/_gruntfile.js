/* <%= title %> */

/*global module:false*/
module.exports = function(grunt) {
	var path = require('path');
	require('load-grunt-tasks')(grunt);

	var options = {
		title: '<%= title %>',
		config : {
			src: 'grunt/**/*'
		},
		app: '<%= appDir %>',
		dist: 'dist',
		resources: 'resources',
		url: '<%= url %>',
    	browserSyncHtmlSrc: '<%= browserSyncHtmlSrc %>',
    	headPartial: '<%= headPartial %>',
    	footPartial: '<%= footPartial %>'
	};

	var configs = require('load-grunt-configs')(grunt, options);

	grunt.initConfig(configs);

	grunt.registerTask('img', ['imagemin', 'cwebp', 'svgmin:images']);

	grunt.registerTask('icons', ['fileregexrename', 'svgcss']);

	grunt.registerTask('build', ['<%- gruntBuildTasks %>']);

	grunt.registerTask('styleguide', ['build', 'exec:styleguide_sass', 'kss']);

	grunt.registerTask('default', ['build',  'browserSync', 'watch']);
};
