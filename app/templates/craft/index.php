<?php

// Path to your craft/ folder
$craftPath = '../craft';

// Do not edit below this line
$path = rtrim($craftPath, '/').'/app/index.php';

if (!is_file($path))
{
	if (function_exists('http_response_code'))
	{
		http_response_code(503);
	}

	exit('Could not find your craft/ folder. Please ensure that <strong><code>$craftPath</code></strong> is set correctly in '.__FILE__);
}

define('CRAFT_CONFIG_PATH', realpath(dirname(__FILE__) . "/../craft/config").'/');
define('CRAFT_TEMPLATES_PATH', realpath(dirname(__FILE__) . "/../craft/templates").'/');
define('CRAFT_PLUGINS_PATH', realpath(dirname(__FILE__) . "/../craft/plugins").'/');

require_once $path;
