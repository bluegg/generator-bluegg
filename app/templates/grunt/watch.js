module.exports = {
  twig: {
    files: ["<%= resources %>/markup/**/*.twig", "<%= resources %>/data/data.json", "craft/templates/**/*.twig"],
    tasks: ["twigRender"]
  },
  sass: {
    files: ["<%= resources %>/styles/scss/**/*.scss"],
    tasks: [
      "sass_globbing:dist",
      "sass:dist",
      "postcss:dist",
      "cache-busting:css"
    ]
  },
  js: {
    files: ["<%= resources %>/scripts/**/*.js"],
    tasks: ["browserify:dist", "exorcise", "uglify", "cache-busting:js"]
  },
  img: {
    files: ["<%= resources %>/images/**/*.{png,jpg,gif}"],
    tasks: ["newer:imagemin", "cwebp"]
  },
  svgimg: {
    files: ["<%= resources %>/images/**/*.{svg}"],
    tasks: ["svgmin"]
  }
};
