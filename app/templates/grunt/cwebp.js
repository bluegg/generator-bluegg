module.exports = {
	dist: {
        files: [{
            expand: true,
            cwd: '<%= app %>/images/',
            src: ['**/*.{png,jpg,gif}'],
            dest: '<%= app %>/images/'
        }]
	}
};
