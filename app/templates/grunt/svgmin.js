module.exports = {
  icons: {
		options: {
			plugins: [
				{ 'removeXMLProcInst': false }
			]
		},
		files: [{
			expand: true,
			cwd: '<%= resources %>/images/icons',
			src: [
				'*.svg'
			],
			dest: '<%= resources %>/images/icons'
		}]
	},
	images: {
		options: {
			plugins: [
				{ 'removeXMLProcInst': false }
			]
		},
		files: [{
			expand: true,
			cwd: '<%= resources %>/images',
			src: [
				'*.svg'
			],
			dest: '<%= app %>/images'
		}]
	}
};
