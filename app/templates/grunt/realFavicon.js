module.exports = {
  favicons: {
      src: "<%= resources %>/favicon.png",
      dest: "<%= app %>",
      options: {
        iconsPath: "/",
        html: ["<%= resources %>/markup/_partials/favicon.twig"], // CHANGE TO PARTIAL LOCATION
        design: {
          ios: {
            pictureAspect: "noChange",
            assets: {
              ios6AndPriorIcons: false,
              ios7AndLaterIcons: false,
              precomposedIcons: false,
              declareOnlyDefaultIcon: true
            }
          },
          desktopBrowser: {},
          windows: {
            pictureAspect: "noChange",
            backgroundColor: "#128dfb", // CHANGE TO THEME COLOR
            onConflict: "override",
            assets: {
              windows80Ie10Tile: false,
              windows10Ie11EdgeTiles: {
                small: false,
                medium: true,
                big: false,
                rectangle: false
              }
            }
          },
          androidChrome: {
            pictureAspect: "noChange",
            themeColor: "#128dfb", // CHANGE TO THEME COLOR
            manifest: {
              name: "<%= title %>",
              display: "standalone",
              orientation: "notSet",
              onConflict: "override",
              declared: true
            },
            assets: {
              legacyIcon: false,
              lowResolutionIcons: false
            }
          },
          safariPinnedTab: {
            pictureAspect: "blackAndWhite",
            threshold: 72.65625,
            themeColor: "#128dfb" // CHANGE TO THEME COLOR
          }
        },
        settings: {
          scalingAlgorithm: "Mitchell",
          errorOnImageTooSmall: false,
          readmeFile: false,
          htmlCodeFile: false,
          usePathAsIs: false
        }
      }
  }
};
