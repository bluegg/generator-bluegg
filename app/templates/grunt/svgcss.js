module.exports = {
	dist: {
		files: {
			'<%= resources %>/styles/scss/_svg.scss': ['<%= resources %>/images/icons/*.svg']
		},
		options: {
			cssprefix: '',
			previewhtml: null
		}
	}
};