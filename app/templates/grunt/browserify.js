module.exports = {
	dist: {
		files: {
			'<%= resources %>/scripts/app.bundle.js': '<%= resources %>/scripts/app.js'
		},
		options: {
			browserifyOptions: {
				debug: true
			},
			transform: [
				['babelify', {
					presets: ["es2015"]
				}]
			],
			plugin: ['exorcist']
		}
	}
};
