module.exports = {
	dist: {
		files: {
    "<%= resources %>/styles/scss/_atoms.scss": "<%= resources %>/styles/scss/atoms/**/*.scss",
    "<%= resources %>/styles/scss/_molecules.scss": "<%= resources %>/styles/scss/molecules/**/*.scss",
		"<%= resources %>/styles/scss/_organisms.scss": "<%= resources %>/styles/scss/organisms/**/*.scss"
		}
	}
};
