module.exports = {
	build: {
		options: {
			// Target specific options go here
			base: '<%= resources %>/markup/_partials/'
		},
		files: [
			{
				data: ['<%= resources %>/data/data.json'],
				cwd: '<%= resources %>/markup',
				expand: true,
				src: ['**/*.twig', '!**/_partials/**'],
				dest: '<%= app %>',
				ext: '.html'
			}
		]
	}
};