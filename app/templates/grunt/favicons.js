module.exports = {
	options: {
		html: "<%= resources %>/partials/favicon.html"
	},
	your_target: {
		src: "<%= resources %>/favicon.png",
		dest: "<%= app %>"
	}
};
