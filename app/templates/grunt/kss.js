module.exports = {
  options: {
    verbose: true,
    title: "<%= title %> Brand and Web Guide",
    builder: "bluegg-kss-builder",
    extend: "node_modules/bluegg-kss-builder/extend",
    custom: ["Colors", "Logos", "Samples"],
    css: "/styles/style.min.css"
  },
  dist: {
    src: [
      "<%= resources %>/styles/scss",
      "<%= resources %>/markup/_partials",
      "craft/templates"
    ],
    dest: "<%= app %>/styleguide"
  }
};
