module.exports = {
	dist: {
		files: {
			"<%= app %>/styles/style.min.css": "<%= resources %>/styles/scss/style.scss"
		},
		options: {
			sourceMap: true,
			sourceMapContents: true,
			quiet: true
		}
	}
};
