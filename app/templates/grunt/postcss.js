module.exports = {
	options: {
		map: true,
		processors: [
			require('autoprefixer')({browsers: ['last 2 version', 'ie 10'], grid: false}),
			require('cssnano')()
		]
	},
	dist: {
		expand: true,
		flatten: true,
		src: ['<%= app %>/styles/style.min.css','<%= app %>/styles/style.css'],
		dest: '<%= app %>/styles/'
	}
};
