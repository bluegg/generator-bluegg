module.exports = {
  fonts: {
    files: [{
      expand: true,
      cwd: '<%= resources %>',
      src: 'fonts/**/*',
      dest: '<%= app %>/'
    }]
  },
  modernizr: {
    files: [{
      expand: true,
      cwd: '<%= resources %>/scripts/vendor',
      src: 'modernizr.min.js',
      dest: '<%= app %>/scripts'
    }]
  }
};
