module.exports = {
  bundle: {
    options: {},
    files: {
      '<%= resources %>/scripts/app.bundle.js.map': ['<%= resources %>/scripts/app.bundle.js']
    }
  }
}
