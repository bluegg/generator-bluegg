module.exports = {
  dist: {
    options: {
      sourceMap: true,
      sourceMapIn: '<%= resources %>/scripts/app.bundle.js.map', // input sourcemap from a previous compilation
    },
    files: {
      '<%= app %>/scripts/app.min.js': ['<%= resources %>/scripts/app.bundle.js'],
    }
  }
}
