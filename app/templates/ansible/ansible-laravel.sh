rm -rf ansible/site-setup-static.yml
rm -rf ansible/cleanup-static.yml
rm -rf ansible/deploy-static.yml

rm -rf ansible/site-setup-craft.yml
rm -rf ansible/cleanup-craft.yml
rm -rf ansible/deploy-craft.yml

rm -rf ansible/assets-pull.yml
rm -rf ansible/assets-push.yml

mv ansible/site-setup-laravel.yml ansible/site-setup.yml
mv ansible/cleanup-laravel.yml ansible/cleanup.yml
mv ansible/deploy-laravel.yml ansible/deploy.yml

cp ansible/inventory.laravel ansible/production
cp ansible/inventory.laravel ansible/staging

rm -rf ansible/inventory.craft
rm -rf ansible/inventory.laravel
rm -rf ansible/inventory.static