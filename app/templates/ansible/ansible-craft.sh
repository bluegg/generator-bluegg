rm -rf ansible/site-setup-static.yml
rm -rf ansible/cleanup-static.yml
rm -rf ansible/deploy-static.yml

rm -rf ansible/site-setup-laravel.yml
rm -rf ansible/cleanup-laravel.yml
rm -rf ansible/deploy-laravel.yml

mv ansible/site-setup-craft.yml ansible/site-setup.yml
mv ansible/cleanup-craft.yml ansible/cleanup.yml
mv ansible/deploy-craft.yml ansible/deploy.yml

cp ansible/inventory.craft ansible/production.example

rm -rf ansible/inventory.craft
rm -rf ansible/inventory.laravel
rm -rf ansible/inventory.static
