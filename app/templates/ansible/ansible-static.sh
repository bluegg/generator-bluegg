rm -rf ansible/site-setup-craft.yml
rm -rf ansible/cleanup-craft.yml
rm -rf ansible/deploy-craft.yml

rm -rf ansible/site-setup-laravel.yml
rm -rf ansible/cleanup-laravel.yml
rm -rf ansible/deploy-laravel.yml

rm -rf ansible/assets-pull.yml
rm -rf ansible/assets-push.yml

mv ansible/site-setup-static.yml ansible/site-setup.yml
mv ansible/cleanup-static.yml ansible/cleanup.yml
mv ansible/deploy-static.yml ansible/deploy.yml

cp ansible/inventory.static ansible/production.example

rm -rf ansible/inventory.craft
rm -rf ansible/inventory.laravel
rm -rf ansible/inventory.static
